@extends('layouts.app')
@section('content')
<h3>{{$movie->title}}</h3>
<p>Release date: {{$movie->release_date}}</p>
<p>{{$movie->opening_crawl}}</p>
@endsection
