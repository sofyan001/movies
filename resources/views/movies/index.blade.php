@extends('layouts.app')
@section('content')
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<div class="row">
    @foreach($movies as $movie)
        <div class="col-sm-4">
            <div class="card">
                @if(in_array($movie->episode_id, $favorites))
                    <div class="img-star">
                        <img src="{{URL::asset('/images/star.png')}}" width="24" height="24" alt="star"/>
                    </div>
                @endif
                <div class="card-body">
                    <h5 class="card-title">{{$movie->title}}</h5>
                    <p class="card-text">{{$movie->opening_crawl}}</p>
                    <a href="{{URL::asset('/movies/' . substr($movie->url, -2))}}">
                        <button class="btn btn-primary btn-margin" type="submit">Bekijk</button>
                    </a>
                    @if(!in_array($movie->episode_id, $favorites))
                        <form method="POST">
                            @csrf
                            <div class="form-group">
                                <input class="form-input" type="hidden" name="id" value="{{$movie->episode_id}}">
                            </div>
                            <a href="{{URL::asset('/movies')}}">
                                <button class="btn btn-primary" type="submit">Toevoegen als favoriet</button>
                            </a>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection
